<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
use Illuminate\Support\Str;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = File::get('database/data/products.json');
        $products = json_decode($data);

        foreach ($products as $product) {
            Product::create([
                'name' => $product,
                'slug' => Str::slug($product),
            ]);
        }

    }
}
