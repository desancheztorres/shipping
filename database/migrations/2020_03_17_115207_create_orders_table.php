<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('order_id', 20);
            $table->string('tracking_number', 11)->nullable();
            $table->string('tracking_url', 100)->nullable();
            $table->string('ip_address', 15)->nullable();
            $table->enum('status', ['not stated', 'pending', 'processing', 'shipped', 'upsell'])->default('not stated');
            $table->dateTime('ordered_on')->nullable();
            $table->dateTime('shipped_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
