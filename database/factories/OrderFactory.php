<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {

    $number = (String) $faker->unique()->numberBetween(00000001, 99999999);

    return [
        'order_id' => 'crm_'.$number,
        'status' => ['not stated', 'pending', 'processing', 'shipped'][rand(0, 3)],
        'ordered_on' => \Carbon\Carbon::now(),
        'created_at' => $faker->dateTimeThisMonth($max = 'now', $timezone = null)->format('Y-m-d H:i:s')
    ];
});
