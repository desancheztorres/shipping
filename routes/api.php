<?php

use Illuminate\Support\Facades\Route;

Route::post('/register', 'Auth\Authcontroller@register');
Route::post('/login', 'Auth\Authcontroller@login');
Route::post('/logout', 'Auth\Authcontroller@logout');

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('/me', 'Auth\Authcontroller@user');
});

Route::group(['prefix' => 'orders', 'middleware' => 'jwt.auth'], function () {
   Route::get('/', 'Orders\OrderController@index');
    Route::get('/{order}/edit', 'Orders\OrderController@edit');
    Route::post('/', 'Orders\OrderController@store');
    Route::patch('/', 'Orders\OrderController@updateStatus');
    Route::patch('/{order}', 'Orders\OrderController@update');
    Route::delete('/{order}', 'Orders\OrderController@destroy');
   Route::delete('/', 'Orders\OrderController@destroyAllOrdersSelected');
});

Route::group(['prefix' => 'products'], function () {
    Route::get('/', 'ProductController@index');
    Route::post('/', 'ProductController@store');
    Route::patch('/{product}', 'ProductController@update');
    Route::delete('/{product}', 'ProductController@destroy');
});

Route::get('logIncomming', 'Orders\OrderController@logIncomming');
