<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ env('APP_NAME') }}</title>

        <link rel="stylesheet" href="css/app.css">

        <!-- Dashboard css -->
        <link href=" {{ asset('assets/css/dashboard.css') }} " rel="stylesheet" />

        <!-- Font family -->
        <link href="//fonts.googleapis.com/css?family=Comfortaa:300,400,700" rel="stylesheet">

        <!--C3 Charts css -->
        <link href=" {{ asset('assets/plugins/charts-c3/c3-chart.css') }} " rel="stylesheet" />

        <!-- Custom scroll bar css-->
        <link href=" {{ asset('assets/plugins/scroll-bar/jquery.mCustomScrollbar.css') }} " rel="stylesheet" />

        <!-- Sidemenu css -->
        <link href=" {{ asset('assets/plugins/toggle-sidebar/sidemenu.css') }} " rel="stylesheet" />

        <!---Font icons css-->
        <link href=" {{ asset('assets/plugins/iconfonts/plugin.css') }} " rel="stylesheet" />
        <link  href=" {{ asset('assets/fonts/fonts/font-awesome.min.css') }} " rel="stylesheet">

    </head>
    <body>
    <!--Global-Loader-->
       <div id="app" class="h-100">
           <app></app>
       </div>

       <script src="js/app.js"></script>

       <!-- Jquery js-->
       <script src=" {{ asset('assets/js/vendors/jquery-3.2.1.min.js') }} "></script>

       <!--Bootstrap js-->
       <script src=" {{ asset('assets/js/vendors/bootstrap.bundle.min.js') }} "></script>

       <!--Jquery Sparkline js-->
       <script src=" {{ asset('assets/js/vendors/jquery.sparkline.min.js') }} "></script>

       <!-- Chart Circle js-->
       <script src=" {{ asset('assets/js/vendors/circle-progress.min.js') }} "></script>

       <!-- Star Rating js-->
       <script src=" {{ asset('assets/plugins/rating/jquery.rating-stars.js') }} "></script>

       <!-- Custom scroll bar js-->
       <script src=" {{ asset('assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js') }} "></script>

    </body>
</html>
