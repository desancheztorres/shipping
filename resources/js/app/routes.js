import auth from './auth/routes'
import home from './home/routes'
import errors from './errors/routes'
import orders from './orders/routes'

export default[...home, ...auth, ...orders, ...errors]
