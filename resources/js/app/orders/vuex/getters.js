export const getHeaders = (state) => {
    return state.headers
}

export const getOrders = (state) => {
    return state.orders
}

export const getOrdersSelected = (state) => {
    return state.ordersSelected
}

export const getOrder = (state) => {
    return state.order
}

export const getStatusSelected = (state) => {
    return state.statusSelected
}
