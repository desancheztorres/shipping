export default {
    headers: [
        {
            text: 'Order #',
            align: 'start',
            value: 'order_id',
        },
        { text: 'Order Date', value: 'ordered_on' },
        { text: 'Name', value: 'name' },
        { text: 'email', value: 'email' },
        { text: 'Postcode', value: 'postcode' },
        { text: 'Status', value: 'status' },
        { text: 'Tracking #', value: 'tracking_number' },
        { text: 'Actions', value: 'actions' },
    ],
    orders: [],
    ordersSelected: [],
    order: null,
    statusSelected: null,
}
