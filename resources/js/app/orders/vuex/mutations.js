export const setOrdersData = (state, data) => {
    state.orders = data
}

export const setOrder = (state, data) => {
    state.order = data
}

export const setOrdersSelected = (state, data) => {
    state.ordersSelected = data
}

export const setStatusSelected = (state, status) => {
    state.statusSelected = status
}

export const removeOrder = (state, id) => {
    const orderExisting = state.orders.find((item) => {
        return item.id === id
    })

    if(orderExisting) {
        state.orders = state.orders.filter((item) => {
            return item.id !== id
        })

        state.ordersSelected = state.ordersSelected.filter((item) => {
            return item.id !== id
        })
    }

}

export const updateStatus = (state, payload) => {

    payload.orders.forEach((order, index) => {
        const orderExisting = state.orders.find((item) => {
            return item.id === order.id
        })

        if(orderExisting) {
            orderExisting.status = payload.status
        }

    })

    state.ordersSelected = []
}

export const removeOrdersSelected = (state, orders) => {
    state.orders = state.orders.filter( ( el ) => !orders.includes( el ) )
    state.ordersSelected = state.ordersSelected.filter((el) => !orders.includes(el))
}

