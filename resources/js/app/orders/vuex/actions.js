import axios from 'axios'

//get products
export const getOrders = ({ commit }, payload) => {
    return axios.get('/api/orders', {
        params: payload
    }).then((response) => {
        commit('setOrdersData', response.data.data)
        return Promise.resolve()
    })
}

export const getOrder = ({ commit }, payload) => {
    console.log(payload)
    return axios.get(`/api/orders/${payload.id}/edit`
    ).then((response) => {
        commit('setOrder', response.data.data)
        return Promise.resolve()
    })
}

export const getOrdersSelected = ({ commit }, data) => {
    commit('setOrdersSelected', data)
}

export const createOrder = ({ commit, dispatch }, { payload, context}) => {
    return axios.post('/api/orders',  payload,{
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    }).then((response) => {
        context.errors = []
        dispatch('flashMessage', 'Order created succesfully', { root: true })
    }).catch((errors) => {
        context.errors = errors.response.data.errors
    })
}

export const updateOrder = ({ commit, dispatch }, { payload, context}) => {

    return axios.patch(`/api/orders/${payload.id}`,  payload,{
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    }).then((response) => {
        console.log(response)
        context.errors = []
        dispatch('flashMessage', 'Order edited succesfully', { root: true })
    }).catch((errors) => {
        context.errors = errors.response.data.errors
    })
}

export const updateStatus = ({ commit, dispatch }, payload) => {

    return axios.patch('/api/orders',  payload,{
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    }).then((response) => {
        commit('updateStatus', payload)
        dispatch('flashMessage', `Orders marked as ${payload.status}`, { root: true })
    }).catch((errors) => {
        console.log(errors)
    })
}

export const deleteOrder = ({ commit }, id) => {
    commit('removeOrder', id)
    return axios.delete(`/api/orders/${id}`).then((response) => {
        console.log(response)
    }).catch((error) => {
        console.log(error)
    })
}

export const deleteOrdersSelected = ({ commit }, {payload, orders}) => {
    commit('removeOrdersSelected', payload)

    return axios.delete(`/api/orders`, {
        params: orders,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    }).then((response) => {
        console.log(response.data)
    }).catch((error) => {
        console.log(error)
    })
}

