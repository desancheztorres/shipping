import Vue from 'vue'

export const MultiSelectInput = Vue.component('multi-select-input', require('./inputs/MultiSelectInput').default)
