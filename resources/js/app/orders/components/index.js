import Vue from 'vue'

export const Order = Vue.component('order', require('./Order').default)
export const OrderCreate = Vue.component('order-create', require('./OrderCreate').default)
export const OrderEdit = Vue.component('order-edit', require('./OrderEdit').default)
export const Table = Vue.component('order-table', require('./OrderTable').default)
export const SearchForm = Vue.component('search-form', require('./SearchForm').default)
export const ActionButton = Vue.component('action-button', require('./ActionButton').default)
