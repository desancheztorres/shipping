import { Order, OrderCreate, OrderEdit } from '../components'

export default [
    {
        path: '/',
        redirect: '/orders'
    },
    {
        path: '/orders',
        component: Order,
        name: 'orders',
        meta: {
            needsAuth: true,
        }
    },
    {
        path: '/order/create',
        component: OrderCreate,
        name: 'order-create',
        meta: {
            needsAuth: true,
        }
    },
    {
        path: '/order/:id/edit',
        component: OrderEdit,
        name: 'order-edit',
        meta: {
            needsAuth: true,
        }
    }
]
