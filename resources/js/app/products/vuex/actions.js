import axios from 'axios'

//get products
export const getProducts = ({ commit }) => {
    return axios.get('/api/products').then((response) => {
        commit('setProductsData', response.data)
        return Promise.resolve()
    })
}
