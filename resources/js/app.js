import router from "./router";
import store from './vuex'
import localforage from 'localforage'
import VueSweetalert2 from 'vue-sweetalert2';
import DisableAutocomplete from 'vue-disable-autocomplete';

import Default from './layouts/Default.vue'
import Auth from './layouts/Auth.vue'

localforage.config({
    driver: localforage.LOCALSTORAGE,
    storeName: 'desancheztorres'
});

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vuetify from 'vuetify';
Vue.use(Vuetify);
Vue.use(VueSweetalert2);
Vue.use(DisableAutocomplete);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/App.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('app', require('./components/App.vue').default);
// Vue.component('navigation', require('./components/Navigation.vue').default);
Vue.component('sidebar-component', require('./components/Sidebar.vue').default);
Vue.component('footer-component', require('./components/Footer.vue').default);
Vue.component('breadcrumb-component', require('./components/Breadcrumb.vue').default);

Vue.component('default-layout', require('./layouts/Default.vue').default);
Vue.component('auth-layout', require('./layouts/Auth.vue').default);

store.dispatch('auth/setToken').then(() => {
    store.dispatch('auth/fetchUser').catch(() => {
        store.dispatch('auth/clearAuth')
        router.replace({ name: 'login' })
    })
}).catch(() => {
    store.dispatch('auth/clearAuth')
})

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    vuetify: new Vuetify(),
    router: router,
    store: store,
    el: '#app',
});
