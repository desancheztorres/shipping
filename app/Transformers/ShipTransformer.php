<?php

namespace App\Transformers;

use App\Models\Ship;
use League\Fractal\TransformerAbstract;

class ShipTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Ship $ship)
    {
        return [
            'id' => $ship->id,
            'name' => $ship->name,
            'lastname' => $ship->lastname,
            'email' => $ship->email,
            'phone' => $ship->phone,
            'address1' => $ship->address1,
            'address2' => $ship->address2,
            'city' => $ship->city,
            'state' => $ship->state,
            'postcode' => $ship->postcode,
            'country' => $ship->country,
        ];
    }
}
