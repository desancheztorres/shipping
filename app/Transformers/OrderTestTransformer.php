<?php

namespace App\Transformers;

use App\OrderTest;
use League\Fractal\TransformerAbstract;

class OrderTestTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(OrderTest $order)
    {
        return [
            'id' => $order->id,
            'order_id' => $order->order_id,
            'name' => $order->billFirst.' '.$order->billLast,
            'email' => $order->billEmail,
            'postcode' => $order->billZip,
            'status' => $order->status,
            'tracking_number' => $order->trackingNumber,
            'ordered_on' => $order->ordered_on,
        ];
    }
}
