<?php

namespace App\Transformers;

use App\Models\Order;
use League\Fractal\TransformerAbstract;

class OrderTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'ship',
        'bill',
        'products'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Order $order)
    {
        return [
            'id' => $order->id,
            'order_id' => $order->order_id,
            'status' => $order->status,
            'tracking_number' => $order->trackingNumber,
            'ordered_on' => $order->ordered_on,
        ];
    }

    public function includeShip(Order $order) {
        return $this->item($order->ship, new ShipTransformer);
    }

    public function includeBill(Order $order) {
        return $this->item($order->bill, new BillTransformer);
    }

    public function includeProducts(Order $order) {
        return $this->collection($order->products, new ProductTransformer);
    }
}
