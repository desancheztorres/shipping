<?php

namespace App\Transformers;

use App\Models\Bill;
use League\Fractal\TransformerAbstract;

class BillTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Bill $bill)
    {
        return [
            'id' => $bill->id,
            'name' => $bill->name,
            'lastname' => $bill->lastname,
            'email' => $bill->email,
            'phone' => $bill->phone,
            'address1' => $bill->address1,
            'address2' => $bill->address2,
            'city' => $bill->city,
            'state' => $bill->state,
            'postcode' => $bill->postcode,
            'country' => $bill->country,
        ];
    }
}
