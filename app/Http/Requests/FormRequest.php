<?php

    namespace App\Http\Requests;

    use Illuminate\Contracts\Validation\Validator;
    use Illuminate\Http\Exceptions\HttpResponseException;

    class FormRequest extends \Illuminate\Foundation\Http\FormRequest {
        /**
         * Handle a failed validation attempt.
         *
         * @param  \Illuminate\Contracts\Validation\Validator  $validator
         * @return void
         *
         * @throws \Illuminate\Validation\ValidationException
         */
        protected function failedValidation(Validator $validator)
        {
            if ($this->expectsJson()) {
                throw new HttpResponseException(response()->json(
                    [
                        'errors' => $validator->errors()
                    ]
                    , 422)
                );
            }

            parent::failedValidation($validator);
        }
    }
