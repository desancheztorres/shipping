<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLogIncomingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id' => 'required|max:11',
            'trackingnumber' => 'required|max:16',
            'status' => 'in:not stated,pending,processing,shipped',
            'order_date_time' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'shipping_address' => 'required',
            'shipping_address_2' => 'required',
            'shipping_city' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'shipping_state_desc' => 'required',
            'billing_address' => 'required',
            'billing_address_2' => 'required',
            'billing_city' => 'required',
            'billing_state_desc' => 'required',
            'billing_state_desc' => 'required',
            'product_names_csv' => 'required',
        ];
    }
}
