<?php

namespace App\Http\Requests\Orders;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shipFirst' => 'required',
            'shipLast' => 'required',
            'shipEmail' => 'required',
            'shipPhone' => 'required',
            'shipAddress1' => 'required',
            'shipAddress2' => 'required',
            'shipCity' => 'required',
            'shipState' => 'required',
            'shipPostcode' => 'required',
            'billFirst' => 'required',
            'billLast' => 'required',
            'billEmail' => 'required',
            'billPhone' => 'required',
            'billAddress1' => 'required',
            'billAddress2' => 'required',
            'billCity' => 'required',
            'billState' => 'required',
            'billPostcode' => 'required',
        ];
    }
}
