<?php

namespace App\Http\Controllers;

use App\Http\Requests\Products\{StoreProductRequest, UpdateProductRequest};
use App\Models\Product;
use App\Transformers\ProductTransformer;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function index() {

        $products = Product::get();

        $products_name = array();

        foreach ($products as $product) {
            array_push($products_name, $product->name);
//            $products_name[$product->id] = $product->name;
        }


        return $products_name;
//
//        return fractal()
//            ->collection($products)
//            ->transformWith(new ProductTransformer)
//            ->toArray();
    }

    public function show(Product $product) {

        return fractal()
            ->item($product)
            ->transformWith(new ProductTransformer)
            ->toArray();
    }

    public function store(StoreProductRequest $request) {

        $product = new Product;
        $product->name = $request->name;
        $product->slug = Str::slug($product->name);

        $product->save();

        return fractal()
            ->item($product)
            ->transformWith(new ProductTransformer)
            ->toArray();
    }

    public function update(UpdateProductRequest $request, Product $product) {

        $product->name = $request->get('name', $product->name);
        $product->slug = Str::slug($product->name);

        $product->save();

        return fractal()
            ->item($product)
            ->transformWith(new ProductTransformer)
            ->toArray();
    }

    public function destroy(Product $product) {
        $product->delete();

        return response(null, 204);
    }
}
