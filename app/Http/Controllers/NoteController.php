<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NoteController extends Controller
{

    public function create()
    {
        echo 'Create';
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $note = new Note;
        $note->user_id = $request->user()->id;
        $note->order_id = $request->order_id;
        $note->note = $request->note;
        $note->save();

        $log = new DatabaseLogging();
        $log->logNotes($request->order_id, $request->note, $request->user()->name);

        return back();

    }
}
