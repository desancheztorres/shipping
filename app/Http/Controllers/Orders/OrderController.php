<?php

namespace App\Http\Controllers\Orders;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests\Orders\{StoreOrderRequest, UpdateOrderRequest};
use App\Models\{Order, Product, Ship, Bill};
use App\Transformers\OrderTransformer;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\StoreLogIncomingRequest;
use Illuminate\Database\Eloquent\Builder;

class OrderController extends Controller
{
    public function index(Request $request) {

        $filters = [
            'from' => $request->get('from'),
            'to' => $request->get('to'),
            'order_id' => $request->get('order_id'),
            'products' => $request->get('products'),
            'tracking_number' => $request->get('tracking_number'),
            'shipFirst' => $request->get('shipFirst'),
            'shipLast' => $request->get('shipLast'),
            'shipEmail' => $request->get('shipEmail'),
            'shipAddress1' => $request->get('shipAddress1'),
            'shipAddress2' => $request->get('shipAddress2'),
            'shipCity' => $request->get('shipCity'),
            'shipState' => $request->get('shipState'),
            'shipPostcode' => $request->get('shipPostcode'),
            'shipPhone' => $request->get('shipPhone'),
        ];

        $orders = Order::with('ship', 'bill', 'products')->where(function ($query) use ($filters) {

            if($filters['from'] && $filters['to']) {
                $query->whereBetween('created_at', array($filters['from'].' 00:00:00', $filters['to'].' 23:59:59'));
            } else {
                $query->whereDate('created_at', Carbon::today())->get();
            }

            if($filters['order_id']) {
                $query->where('order_id', $filters['order_id']);
            }

            if($filters['tracking_number']) {
                $query->where('tracking_number', $filters['tracking_number']);
            }

            if($filters['shipFirst']) {
                $query->whereHas('ship', function($q) use ($filters) {
                    $q->where('name', $filters['shipFirst']);
                });
            }

            if($filters['shipLast']) {
                $query->whereHas('ship', function($q) use ($filters) {
                    $q->where('lastname', $filters['shipLast']);
                });
            }

            if($filters['shipAddress1']) {
                $query->whereHas('ship', function($q) use ($filters) {
                    $q->where('address1', $filters['shipAddress1']);
                });
            }

            if($filters['shipAddress2']) {
                $query->whereHas('ship', function($q) use ($filters) {
                    $q->where('address2', $filters['shipAddress2']);
                });
            }

            if($filters['shipCity']) {
                $query->whereHas('ship', function($q) use ($filters) {
                    $q->where('city', $filters['shipCity']);
                });
            }

            if($filters['shipState']) {
                $query->whereHas('ship', function($q) use ($filters) {
                    $q->where('state', $filters['shipState']);
                });
            }

            if($filters['shipPostcode']) {
                $query->whereHas('ship', function($q) use ($filters) {
                    $q->where('postcode', $filters['shipPostcode']);
                });
            }

            if($filters['shipPhone']) {
                $query->whereHas('ship', function($q) use ($filters) {
                    $q->where('phone', $filters['shipPhone']);
                });
            }

            if($filters['shipEmail']) {
                $query->whereHas('ship', function($q) use ($filters) {
                    $q->where('email', $filters['shipEmail']);
                });
            }

            if($filters['products']) {
                $query->whereHas('products', function($q) use ($filters) {
                    $q->whereIn('name', $filters['products']);
                });
            }

        })->get();

        return fractal()
            ->collection($orders)
            ->parseIncludes(['ship', 'bill', 'products'])
            ->transformWith(new OrderTransformer)
            ->toArray();
    }

    public function store(StoreOrderRequest $request) {

        $products = Product::select('id')->whereIn('name', $request->get('products'))->get();

        $all_products_ids = array();

        foreach ($products as $product) {

            array_push($all_products_ids, $product->id);
        }

        $order = new Order;

        $order->order_id = $order->getPrefix().$order->generateOrderId();
        $order->status = 'not stated';

        $ship = new Ship;
        $ship->name = $request->shipFirst;
        $ship->lastname = $request->shipLast;
        $ship->email = $request->shipEmail;
        $ship->phone = $request->shipPhone;
        $ship->address1 = $request->shipAddress1;
        $ship->address2 = $request->shipAddress2;
        $ship->city = $request->shipCity;
        $ship->state = $request->shipState;
        $ship->postcode = $request->shipPostcode;

        $bill = new Bill;
        $bill->name = $request->billFirst;
        $bill->lastname = $request->billLast;
        $bill->email = $request->billEmail;
        $bill->phone = $request->billPhone;
        $bill->address1 = $request->billAddress1;
        $bill->address2 = $request->billAddress2;
        $bill->city = $request->billCity;
        $bill->state = $request->billState;
        $bill->postcode = $request->billPostcode;

        $order->save();
        $order->ship()->save($ship);
        $order->bill()->save($bill);
        $order->products()->sync($all_products_ids, false);

        return fractal()
            ->item($order)
            ->parseIncludes(['bill', 'ship', 'products'])
            ->transformWith(new OrderTransformer)
            ->toArray();
    }

    public function edit(Order $order) {
        return fractal()
            ->item($order)
            ->parseIncludes(['bill', 'ship', 'products'])
            ->transformWith(new OrderTransformer)
            ->toArray();
    }

    public function update(UpdateOrderRequest $request, Order $order) {

        $products = Product::select('id')->whereIn('name', $request->get('products'))->get();

        $all_products_ids = array();

        foreach ($products as $product) {

            array_push($all_products_ids, $product->id);
        }

        $order->ship()->update([
            'name' => $request->shipFirst,
            'lastname' => $request->shipLast,
            'email' => $request->shipEmail,
            'phone' => $request->shipPhone,
            'address1' => $request->shipAddress1,
            'address2' => $request->shipAddress2,
            'city' => $request->shipCity,
            'state' => $request->shipState,
            'postcode' => $request->shipPostcode,
        ]);

        $order->bill()->update([
            'name' => $request->billFirst,
            'lastname' => $request->billLast,
            'email' => $request->billEmail,
            'phone' => $request->billPhone,
            'address1' => $request->billAddress1,
            'address2' => $request->billAddress2,
            'city' => $request->billCity,
            'state' => $request->billState,
            'postcode' => $request->billPostcode,
        ]);

        $order->products()->sync($all_products_ids);

        return fractal()
            ->item($order)
            ->parseIncludes(['bill', 'ship', 'products'])
            ->transformWith(new OrderTransformer)
            ->toArray();
    }

    public function destroy(Order $order) {
        $order->delete();

        return response(null, 204);
    }

    public function updateStatus(Request $request) {

        if($request->get('orders')) {
            $all_orders = array();

            foreach($request->get('orders') as $order) {
                array_push($all_orders, $order['id']);
            }

            Order::whereIn('id', $all_orders)->update([
               'status' => $request->get('status'),
            ]);

            return response(null, 200);
        }
    }

    public function destroyAllOrdersSelected(Request $request) {

        Order::whereIn('id', $request->orders)->delete();

        return response(null, 204);
    }

    public function logIncomming(StoreLogIncomingRequest $request) {

        $email = $request->get('email');
        $date = $request->get('order_date');

        $productName = $request->get('product_names_csv');

        $product = Product::select('id')->where('name', $productName)->first();

        if(is_null($product)) {
            $product = new Product;
            $product->name = $productName;
            $product->save();
        }

//        $status = null;
//
//        switch($request->get('order_status')) {
//            case 1:
//                $status = 'processing';
//                break;
//            case 2:
//                $status = 'shipped';
//                break;
//            default :
//                $status = 'pending';
//        }

        $order = new Order;


        // We check if the order contains the same email and has been ordered today
        $order->dupe($date, $email);

        $order->order_id = $request->get('order_id');
        $order->tracking_number = $request->get('trackingnumber');
        $order->status = $order->orderStatus;
        $order->ordered_on = date('Y-m-d', strtotime($request->get('order_date_time')));

        $ship = new Ship;
        $ship->name = $request->get('first_name');
        $ship->lastname = $request->get('last_name');
        $ship->address1 = $request->get('shipping_address');
        $ship->address2 = $request->get('shipping_address_2');
        $ship->city = $request->get('shipping_city');
        $ship->email = $request->get('email');
        $ship->phone = $request->get('phone');
        $ship->state = $request->get('shipping_state_desc');
        $ship->postcode = $request->get('shipping_state_desc');

        $bill = new Bill;
        $bill->name = $request->get('first_name');
        $bill->lastname = $request->get('last_name');
        $bill->address1 = $request->get('billing_address');
        $bill->address2 = $request->get('billing_address_2', $request->get('billing_address'));
        $bill->city = $request->get('billing_city');
        $bill->email = $request->get('email');
        $bill->phone = $request->get('phone');
        $bill->state = $request->get('billing_state_desc');
        $bill->postcode = $request->get('billing_state_desc');

        $order->save();
        $order->ship()->save($ship);
        $order->bill()->save($bill);

        if($order->orderStatus === 'upsell') {
            $getOriginalOrder = $order->getTodayOrderByEmail($date, $email);

            $getOriginalOrder->products()->attach($product->id);

        } else {
            $order->products()->sync($product->id, false);
        }

        Log::debug($request);

    }
}
