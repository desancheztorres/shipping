<?php

namespace App\Http\Controllers\Orders;

use App\Models\Order;
use Illuminate\Http\Request;
use Mail;

class ConfirmationController extends Controller
{
    public function shipped($id){

        $order = Order::find($id);
        $tracking = $order->trackingNumber;

        Mail::to($order->shipEmail)->send(new OrderShipped($order, $tracking));

//        return redirect()->back()->with('shipped', 'Shipping Confirmation Sent');
        return $order;


    }

    public function confirmation($id){

        $order = Order::find($id);
        Mail::to($order->shipEmail)->send(new OrderConfirmation($order));

        return $order;


    }
}
