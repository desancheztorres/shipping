<?php

namespace App\Hermes;

use App\Hermes\Hermes;
use App\Models\{Product, Order};
use DOMDocument;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Dompdf\Dompdf;
use Carbon\Carbon;
use App\Limelight\Api;
use Log;

class Labels extends Hermes {


    public function createLabel($res, $orderid){

        try {

            $doc = new DOMDocument();
            $doc->loadXML($res);
            $label = $doc->getElementsByTagName('labelImage')->item(0)->nodeValue;
            $barcode = $doc->getElementsByTagName('barcodeNumber')->item(0)->nodeValue;
            $image = base64_decode($label);
            $fp = fopen (public_path().'/labels/'.$orderid.'.pdf','w');
            fwrite($fp,$image);
            fclose($fp);



            //Update order table
            DB::table('orders')->where('order_id',$orderid)->update(['trackingNumber' => $barcode, 'status' => 'processing', 'shipped_on' => Carbon::now()]);

            Log::debug('HERMES LABEL CREATED FOR ORDERID: '.$orderid.' | TRACKING NUMBER FROM HERMES: '.$barcode);

            $limelight = new Api;
            $limelight->updateLimelight($orderid, $barcode);



        } catch (Exception $e) {
            report($e);


            return false;
        }

    }

    public function singleDispatchNote($customer, $products){

        try {

            $pdf = \App::make('dompdf.wrapper');
            $pdf = \PDF::loadView('orders.dispatchnote', compact('customer','products'))
                ->save(public_path('dispatch/'.$customer->order_id.'.pdf'));

        } catch (Exception $e) {
            report($e);

            return false;
        }

    }

    public function generateSingleDispatchNote($orderid){

        try {

            $customer = Order::where('order_id',$orderid)->first();
            $products = Product::where('order_id',$orderid)->get();

            $pdf = \App::make('dompdf.wrapper');
            $pdf = \PDF::loadView('orders.dispatchnote', compact('customer','products'))
                ->save(public_path('dispatch/'.$customer->order_id.'.pdf'));



        } catch (Exception $e) {
            report($e);

            return false;
        }

    }

    public function refreshDispatchNote(){

        ini_set('memory_limit','8024M');

        try {

            $orders= Order::whereDate('created_at', Carbon::today())->get();

            foreach($orders as $order){

                $products = Product::where('order_id',$order->order_id)->get();
                $customer = $order;

                $pdf = \App::make('dompdf.wrapper');
                $pdf = \PDF::loadView('orders.dispatchnote', compact('customer','products'))
                    ->save(public_path('dispatch/'.$order->order_id.'.pdf'));

            }

            echo 'complete';


        } catch (Exception $e) {
            report($e);

            return false;
        }

    }

    public function refreshDispatchNoteAll($date){

        ini_set('memory_limit','18024M');

        try {

            $orders= Order::whereDate('created_at', $date)->get();

            foreach($orders as $order){

                $products = Product::where('order_id',$order->order_id)->get();
                $customer = $order;

                $pdf = \App::make('dompdf.wrapper');
                $pdf = \PDF::loadView('orders.dispatchnote', compact('customer','products'))
                    ->save(public_path('dispatch/'.$order->order_id.'.pdf'));

            }

            echo 'complete';


        } catch (Exception $e) {
            report($e);

            return false;
        }

    }

    public function createMultipleDispatchNotes($customers){

        ini_set('memory_limit','8024M');

        try {


            $pdf = new \Jurosh\PDFMerge\PDFMerger;

            foreach($customers as $customer){

                $customer = (object) $customer;

                $pdf->addPDF(public_path().'/dispatch/'.$customer->order_id.'.pdf', 'all')
                    ->merge('file', public_path().'/dispatch/MultiDispatch.pdf');


            }


        } catch (Exception $e) {
            report($e);

            return false;
        }

    }

    public function createMultipleLabels($customers){

        ini_set('memory_limit','8024M');

        try {

            $pdf = new \Jurosh\PDFMerge\PDFMerger;

            foreach($customers as $customer){

                $customer = (object) $customer;

                $pdf->addPDF(public_path().'/labels/'.$customer->order_id.'.pdf', 'all')
                    ->merge('file', public_path().'/labels/MultiLabels.pdf');

            }


        } catch (Exception $e) {
            report($e);

            return false;
        }

    }


}
