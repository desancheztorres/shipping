<?php

namespace App\Hermes;

use Carbon\Carbon;
use Log;

class Hermes
{

    public function __construct(){

        $this->url = env('HERMES_URL');
        $this->username = env('HERMES_USERNAME');
        $this->password = env('HERMES_PASSWORD');
        $this->cliendid = env('HERMES_CLIENTID');
        $this->userid = env('USERID');

    }


    public function requestParams($param){

        try {

            $requestParams = array();
            $requestParams[] = $this->date = Carbon::now()->toDateString();
            $requestParams[] = $this->firstname = $param->billFirst;
            $requestParams[] = $this->lastname = $param->billLast;
            $requestParams[] = $this->address1 = $param->shipAddress1;
            $requestParams[] = $this->address2 = $param->shipAddress2;
            $requestParams[] = $this->postcode = $param->shipZip;
            $requestParams[] = $this->town = $param->shipCity;
            $requestParams[] = $this->county = $param->shipState;
            $requestParams[] = $this->home = $param->billPhone;
            $requestParams[] = $this->mobile = $param->billPhone;
            $requestParams[] = $this->email = $param->billEmail;
            $requestParams[] = $this->product = '1';
            $requestParams[] = $this->orderid = $param->order_id;
            $requestParams[] = $this->address1 = preg_replace('/[^0-9]/', '', $this->address1);

            return $requestParams;

        } catch (Exception $e) {
            report($e);

            return false;
        }

    }

    public function buildRequest($param) {

        try {

            $this->date = Carbon::now()->toDateString();
            $this->firstname = $param->billFirst;
            $this->lastname = $param->billLast;
            $this->address1 = $param->shipAddress1;
            $this->address2 = $param->shipAddress2;
            $this->postcode = $param->shipZip;
            $this->town = $param->shipCity;
            $this->county = $param->shipState;
            $this->home = $param->billPhone;
            $this->mobile = $param->billPhone;
            $this->email = $param->billEmail;
            $this->product = '1';
            $this->orderid = $param->orderID;
            $this->address1 = preg_replace('/[^0-9]/', '', $this->address1);

            $this->request = '

    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v3="http://v3.web.domain.routing.hermes.co.uk/">
    <soapenv:Header/>
    <soapenv:Body>
    <v3:routeDeliveryCreatePreadviceReturnBarcodeAndLabel>
    <deliveryRoutingRequest>
    <clientId>857</clientId>
    <clientName>Ultra Health</clientName>
    <batchNumber>1</batchNumber>
    <creationDate>'.$this->date.'</creationDate>
    <userId>857</userId>
    <sourceOfRequest>CLIENTWS</sourceOfRequest>
    <deliveryRoutingRequestEntries>
    <deliveryRoutingRequestEntry>
    <addressValidationRequired>?</addressValidationRequired>
    <customer>
    <address>
    <title></title>
    <firstName>'.$this->firstname.'</firstName>
    <lastName>'.$this->lastname.'</lastName>
    <houseNo></houseNo>
    <streetName>'.$this->address2.'</streetName>
    <countryCode>GB</countryCode>
    <postCode>'.$this->postcode.'</postCode>
    <city>'.$this->town.'</city>
    <addressLine1>'.$this->address1.'</addressLine1>
    <region>'.$this->county.'</region>
    </address>
    <homePhoneNo>'.$this->home.'</homePhoneNo>
    <workPhoneNo>'.$this->home.'</workPhoneNo>
    <mobilePhoneNo>'.$this->mobile.'</mobilePhoneNo>
    <faxNo>'.$this->home.'</faxNo>
    <email>'.$this->email.'</email>
    <customerReference1>Health-Products</customerReference1>
    <customerReference2></customerReference2>
    <customerAlertType>2</customerAlertType>
    <customerAlertGroup>0001</customerAlertGroup>
    <deliveryMessage>None</deliveryMessage>
    <specialInstruction1>None</specialInstruction1>
    <specialInstruction2>None</specialInstruction2>
    </customer>
    <parcel>
    <weight>1</weight>
    <length>10</length>
    <width>10</width>
    <depth>10</depth>
    <girth>10</girth>
    <combinedDimension>10</combinedDimension>
    <volume>2</volume>
    <currency>GBP</currency>
    <value>10</value>
    <numberOfParts>1</numberOfParts>
    <numberOfItems>1</numberOfItems>
    <description>Health Products</description>
    <originOfParcel>Horsham</originOfParcel>
    </parcel>
    <services>
    </services>
    <senderAddress>
    <addressLine1>1st Floor, 15 Carfax</addressLine1>
    <addressLine2>Ridgeland House</addressLine2>
    <addressLine3>Horsham</addressLine3>
    <addressLine4>RH12 1DY</addressLine4>
    </senderAddress>
    <expectedDespatchDate>'.$this->date.'</expectedDespatchDate>
    <countryOfOrigin>GB</countryOfOrigin>
    </deliveryRoutingRequestEntry>
    </deliveryRoutingRequestEntries>
    </deliveryRoutingRequest>
    </v3:routeDeliveryCreatePreadviceReturnBarcodeAndLabel>
    </soapenv:Body>
    </soapenv:Envelope>

    ';

//            Log::debug($this->request);

            return $this->request;

        } catch (Exception $e) {
            report($e);

            return false;
        }

    }

    public function postData($res){

        try {

            $request_headers = array();
//            $request_headers[] = 'POST /routing/service/soap/v3 HTTP/1.1';
            $request_headers[] = 'Content-Type: text/xml;charset=UTF-8';
            $request_headers[] = 'Host: www.hermes-europe.co.uk';
//            $request_headers[] = 'Accept-Encoding: gzip,deflate';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$this->url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_USERPWD, $this->username.':'.$this->password);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$res);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close ($ch);

            return $response;
//            var_dump($response);

        } catch (Exception $e) {
            report($e);

            return false;
        }

    }


    public function hermesLogin(){

        $this->url = env('HERMES_URL');
        $this->username = env('HERMES_USERNAME');
        $this->password = env('HERMES_PASSWORD');
        $this->cliendid = env('HERMES_CLIENTID');
        $this->userid = env('USERID');

    }

}
