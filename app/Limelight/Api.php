<?php

namespace App\Limelight;

use App\Hermes\Hermes;
use App\Hermes\Labels;
use App\Models\{Product, Order, Upsell};
use DOMDocument;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Dompdf\Dompdf;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use App\Mailer\OrderShipped;
use App\Mailer\OrderConfirmation;
use Log;

class Api extends Hermes
{

    public function __construct()
    {

        $this->username = env('LL_USERNAME');
        $this->password = env('LL_PASSWORD');
        $this->url_find = config('app.orderfind');
        $this->url_view = config('app.orderview');
        $this->url_update = config('app.orderupdate');
        $this->dateToday = Carbon::today()->format('m/d/Y');
        $this->dateTodayDupe = Carbon::today()->format('Y-m-d');

    }

    public function getOrders()
    {
        ini_set('memory_limit', '8024M');

        try {

            $data = '
            {
                 "start_date":"'.$this->dateToday.'",
                 "end_date":"'.$this->dateToday.'",
                 "campaign_id":"all",
                 "criteria":{
                 "new":"1",
                 "rma":"0",
                 "has_upsells":"0",
                 "approved":"1"
             },
                "search_type":"all"
                }
            ';

            $request_headers = array();
            $request_headers[] = 'Content-Type: application/json';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->url_find);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_USERPWD, $this->username . ':' . $this->password);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);

            $response = json_decode($response);

            foreach ($response->order_id as $order) {
                echo config('app.res_orderid') . $order.':';
                $this->viewOrders($order);
                echo '<br>';
            }

            echo config('app.res_complete');

        } catch (Exception $e) {
            report($e);

            return false;
        }

    }

    public function getOrdersDate($date)
    {
        ini_set('memory_limit', '8024M');

        try {

            $data = '
            {
                 "start_date":"'.$date.'",
                 "end_date":"'.$date.'",
                 "campaign_id":"all",
                 "criteria":{
                 "new":"1",
                 "rma":"0",
                 "has_upsells":"0",
                 "approved":"1",
                 "shipped" : "1"
             },
                "search_type":"all"
                }
            ';

            $request_headers = array();
            $request_headers[] = 'Content-Type: application/json';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->url_find);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_USERPWD, $this->username . ':' . $this->password);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);

            $response = json_decode($response);

            foreach ($response->order_id as $order) {
                echo config('app.res_orderid') . $order.':';
                $this->viewOrders($order,$date);
                echo '<br>';
            }

            echo config('app.res_complete');

        } catch (Exception $e) {
            report($e);

            return false;
        }

    }

    public function viewOrders($order,$date)
    {

        ini_set('memory_limit', '8024M');

        $data = '
            {
	            "order_id":[
		        ' . $order . '
	            ],
                "return_variants":1
                }
            ';

        $request_headers = array();
        $request_headers[] = 'Content-Type: application/json';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url_view);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $this->username . ':' . $this->password);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        $res = json_decode($response);

        $this->addToOrders($res, $date);
    }

    public function addToOrders($res, $date)
    {

        ini_set('memory_limit', '8024M');

        $date = explode("/",$date);
        $date = $date[2].'-'.$date[0].'-'.$date[1];

        $products = Arr::pluck($res->products, 'is_shippable');
        $product = $products[0];

        // Check if orders are shippable
        if ($product == '1') {

            $validate_id = Order::where('order_id', $res->order_id)->get();

            // Check for duplicate records
            if ($validate_id->isEmpty()) {

//                $validate_email = Order::where('billEmail', $res->email_address)->get();
//                $validate_email = Order::where('billEmail', $res->email_address)->whereDate('created_at', Carbon::today())->get();
                $validate_email = Order::where('billEmail', $res->email_address)->whereDate('created_at', $date)->get();

                // Check if order is an upsale and stack products
                if ($validate_email->isEmpty()) {

                    $order = new Order;
                    $order->order_id = $res->order_id;
                    $order->billFirst = $res->billing_first_name;
                    $order->billLast = $res->billing_last_name;
                    $order->shipEmail = $res->email_address;
                    $order->billEmail = $res->email_address;
                    $order->billPhone = $res->customers_telephone;
                    $order->billAddress1 = $res->billing_street_address;
                    $order->billAddress2 = $res->billing_street_address2;
                    $order->billCity = $res->billing_city;
                    $order->billState = $res->billing_state;
                    $order->billZip = $res->billing_postcode;
                    $order->shipFirst = $res->shipping_first_name;
                    $order->shipLast = $res->shipping_last_name;
                    $order->shipAddress1 = $res->shipping_street_address;
                    $order->shipAddress2 = $res->shipping_street_address2;
                    $order->shipCity = $res->shipping_city;
                    $order->shipState = $res->shipping_state;
                    $order->shipZip = $res->shipping_postcode;
                    $order->shipPhone = $res->customers_telephone;
                    $order->ipAddress = $res->ip_address;
                    $order->status = 'pending';
                    $order->save();

                    $products = Arr::pluck($res->products, 'sku');
                    $product = $products[0];

                    $products = new Product;
                    $products->order_id = $res->order_id;
                    $products->products = $product;
                    $products->save();

                    try {

                        $order = Order::find($order->id);

                        echo config('app.res_addShip');

                        Log::debug('ORDERID ADD TO ORDERS: '.$res->order_id);

                        $this->addToHermes($order, $res->order_id);

                        $dispatch = new Labels();
                        $dispatch->generateSingleDispatchNote($res->order_id);



//                        Mail::to($order->shipEmail)->send(new OrderConfirmation($order));

                    } catch (Exception $e) {
                        report($e);

                        echo 'error';
                    }



                } else {

                    $products = Arr::pluck($res->products, 'sku');
                    $product = $products[0];

                    $order = Order::where('billEmail', $res->email_address)->first();

                    $validate_upsell = Product::where('order_id', $order->order_id)->where('products', $product)->get();

                    if ($validate_upsell->isEmpty()) {

                        $products = new Product;
                        $products->order_id = $order->order_id;
                        $products->products = $product;
                        $products->save();


                        echo config('app.res_addU') . $product;

                    } else {

                        $upsale = Upsell::where('order_id', $res->order_id)->get();

                        if ($upsale->isEmpty()) {

                            $upsell = new Upsell;
                            $upsell->order_id = $res->order_id;
                            $upsell->shipped = 0;
                            $upsell->email = $res->email_address;
                            $upsell->save();

                        }

                        echo config('app.res_dupeU');
                    }


                }

            } else {

                echo config('app.res_dupe');
            }

        } else {
            echo config('app.res_noShip');
        }

    }

    public function addToHermes($res, $id)
    {

        ini_set('memory_limit', '8024M');

        try {

            Log::debug('ORDERID ADD TO HERMES: '.$res->order_id);

            $order = new Hermes();
            $request = $order->buildRequest($res);
            $submit = $order->postData($request);

            $label = new Labels();
            $createLabel = $label->createLabel($submit, $id);


            echo config('app.res_sentH');

        } catch (Exception $e) {
            report($e);

            return false;
        }


    }

    public function createDispatch($data, $products)
    {

        ini_set('memory_limit', '8024M');

        $label = new Labels();
        $label->singleDispatchNote($data, $products);

    }

    public function updateLimelight($id, $tracking)
    {

        ini_set('memory_limit', '8024M');

        try {

            $data = '
            {
              "order_id":
                {
                        "' . $id . '":
                   {
                        "tracking_number":"' . $tracking . '"
                   }
                }
            }

        ';

            Log::debug('LIME LIGHT ORDER TRACKING UPDATED FOR ORDERID: '.$id.' | TRACKING NUMBER USED: '.$tracking);
            Log::info($data);

            $request_headers = array();
            $request_headers[] = 'Content-Type: application/json';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->url_update);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_USERPWD, $this->username . ':' . $this->password);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);

//            print_r($response).'<br>';
            echo config('app.res_updateL');

        } catch (Exception $e) {
            report($e);

            return false;
        }


    }

}
