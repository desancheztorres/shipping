<?php

namespace App\Observers;

use App\Models\Ship;

class ShipObserver
{
    /**
     * Handle the ship "created" event.
     *
     * @param Ship  $ship
     * @return void
     */
    public function created(Ship $ship)
    {
        //
    }
}
