<?php

namespace App\Traits\Order;

trait DupeNewOrder {
    public function dupe($date, $email) {

        $data = $this->getTodayOrderByEmail($date, $email);

        if($data) {
            $this->changeStatus('upsell');
        } else {
            $this->changeStatus('pending');
        }
    }

    public function getTodayOrderByEmail ($date, $email) {
        return $this->whereDate('created_at', $date)->whereHas('bill', function($q) use ($email) {
            $q->where('email', $email);
        })->oldest()->first();
    }

    protected function changeStatus($status) {
        $this->orderStatus = $status;
    }
}
