<?php

    namespace App\Traits\Order;

    trait CreateOrderId {

        function getPrefix() {
            return 'crm_';
        }

        function generateOrderId() {

            $order_id = (int) $this->splitOrderId()[1];

            return $this->increaseOrderId($order_id);
        }

        function getLatestOrderId() {
            $order = $this->latest('order_id')->first();

            return $order->order_id;
        }

        function increaseOrderId($id) {
            return $id+1;
        }

        function splitOrderId() {
            return explode("crm_", $this->getLatestOrderId());
        }
    }
