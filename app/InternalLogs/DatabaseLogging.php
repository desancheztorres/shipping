<?php

namespace App\InternalLogs;

use App\Models\Log;

class DatabaseLogging
{

    public function logActivity($channel, $type, $description, $user){

        $log = new Log;
        $log->channel = $channel;
        $log->type = $type;
        $log->description = $description;
        $log->user = $user;
        $log->save();

    }

    public function logOrderShipped($email, $tracking, $user){

        $log = new Log;
        $log->channel = 'email';
        $log->type = 'shipping confirmation';
        $log->description = 'order confirmation sent to '.$email.' with tracking number '.$tracking;
        $log->user = $user;
        $log->save();

    }

    public function logOrderConfirmation($email, $user){

        $log = new Log;
        $log->channel = 'email';
        $log->type = 'order confirmation';
        $log->description = 'order confirmation sent to '.$email;
        $log->user = $user;
        $log->save();

    }

    public function logHermesSubmit($id, $postcode, $user){

        $log = new Log;
        $log->channel = 'order';
        $log->type = 'hermes';
        $log->description = 'order '.$id. ' sent to Hermes API using postcode '.$postcode;
        $log->user = $user;
        $log->save();

    }

    public function logLabelCreate($id, $user){

        $log = new Log;
        $log->channel = 'order';
        $log->type = 'label';
        $log->description = 'label created for order id '.$id;
        $log->user = $user;
        $log->save();

    }

    public function logDispatchCreate($id, $user){

        $log = new Log;
        $log->channel = 'order';
        $log->type = 'dispatch note';
        $log->description = 'dispatch note created for order id '.$id;
        $log->user = $user;
        $log->save();

    }

    public function logNotes($id, $note, $user){

        $log = new Log;
        $log->channel = 'note';
        $log->type = 'note';
        $log->description = 'note added to orderid '.$id.'. note: '.$note;
        $log->user = $user;
        $log->save();

    }

    public function logDelete($id, $user){

        $log = new Log;
        $log->channel = 'delete';
        $log->type = 'order deleted';
        $log->description = 'orderid '.$id.' deleted by user '.$user;
        $log->user = $user;
        $log->save();

    }

    public function logUpdate($id, $user){

        $log = new Log;
        $log->channel = 'update';
        $log->type = 'order updated';
        $log->description = 'orderid '.$id.' updated by user '.$user;
        $log->user = $user;
        $log->save();

    }

}
