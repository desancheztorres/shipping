<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function order() {
        return $this->belongsTo(Order::class);
    }
}
