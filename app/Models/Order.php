<?php

namespace App\Models;

use App\Traits\Order\{CreateOrderId, DupeNewOrder};
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use CreateOrderId, DupeNewOrder;

    protected $guarded = [];

    public $orderStatus = 'not stated';

    public function ship() {
        return $this->hasOne(Ship::class);
    }

    public function bill() {
        return $this->hasOne(Bill::class);
    }

    public function products() {
        return $this->belongsToMany(Product::class);
    }

}
